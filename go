#!/usr/bin/env ruby

# For vim syntax highlighting:
# :set filetype=ruby

# Run with the command:
# $ ruby -e "$(curl -fsSL https://bitbucket.org/senpo/kitchenplanner/raw/5963aacf18ab282c715738230729f33cffbd0767/go)"

KITCHENPLAN_PATH = ENV.fetch("KITCHENPLAN_PATH", "/opt/kitchenplan")
KITCHENPLAN_REPO = ENV.fetch("KITCHENPLAN_REPO", "https://bitbucket.org/senpo/kitchenplanner.git")

module Tty extend self
  def red; underline 31; end
  def yellow; underline 33 ; end
  def blue; bold 34; end
  def white; bold 39; end

  def reset; escape 0; end
  def bold n; escape "1;#{n}" end
  def underline n; escape "4;#{n}" end
  def escape n; "\033[#{n}m" if STDOUT.tty? end
end

class Array
  def shell_s
    cp = dup
    first = cp.shift
    cp.map{ |arg| arg.gsub " ", "\\ " }.unshift(first) * " "
  end
end

def ohai(*args)
  puts "#{Tty.blue}==>#{Tty.white} #{args.shell_s}#{Tty.reset}"
end

def warn(message)
  puts "#{Tty.yellow}Warning#{Tty.reset}: #{message.chomp}"
end

def die(message)
  puts "#{Tty.red}Critical#{Tty.reset}: #{message.chomp}"
  Kernel.abort
end

def system(*args)
  ohai(*args)
  die("Failed while executing $ #{args.shell_s}") unless Kernel.system(*args)
end

def sudo(*args)
  args = if args.length > 1
    args.unshift "/usr/bin/sudo"
  else
    "/usr/bin/sudo #{args.first}"
  end
  system(*args)
end

def macos_version
  @macos_version ||= `/usr/bin/sw_vers -productVersion`.chomp[/10\.\d+/]
end

def is_admin
  `groups`.split.include? "admin"
end

die "OSX too old, you need at least Mountain Lion" if macos_version < "10.8"
die "Don't run this as root!" if Process.uid == 0
die "The user #{ENV['USER']} is not an Administrator." unless is_admin
die "Please install Command Line Tools with $ xcode-select --install" unless File.exist? "/Library/Developer/CommandLineTools/usr/bin/clang"

ohai("Setting up the Kitchenplan installation...")
sudo("rm", "-rf", "#{KITCHENPLAN_PATH}")
sudo("mkdir", "-p", "#{KITCHENPLAN_PATH}")
sudo("chown", "-R", "#{ENV["USER"]}", "#{KITCHENPLAN_PATH}")
system("git", "clone", "-q", "#{KITCHENPLAN_REPO}", "#{KITCHENPLAN_PATH}")

Dir.chdir(KITCHENPLAN_PATH)
system("./build")
